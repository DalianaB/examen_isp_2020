package examen.subiect2;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class Aplicatie extends JFrame {
    JTextField jTextField1 = new JTextField();
    JTextField jTextField2 = new JTextField();
    JTextField jTextField3 = new JTextField();
    JButton jButton = new JButton();

    Aplicatie() {

        setTitle("Titlul ferestrei");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        init();
        setVisible(true);
    }
    public void init() {
        this.setLayout(null);
        int width=80;int height = 20;

        jTextField1 = new JTextField("Primul");
        jTextField1.setBounds(10,50,width, height);
        jTextField1.setEnabled(true);
        jTextField2 = new JTextField("Al doilea");
        jTextField2.setBounds(10,50,width, height);
        jTextField2.setEnabled(true);
        jTextField3 = new JTextField();
        jTextField3.setBounds(10,50,width, height);
        jTextField3.setEnabled(false);
        jButton = new JButton("Click");
        add(jTextField1);
        add(jTextField2);
        add(jTextField3);
        add(jButton);
    }
}
